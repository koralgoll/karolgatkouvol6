﻿using UnityEngine;
using System.Collections;

public class BulletController : MonoBehaviour {

	public GameObject turret_prefab;
	
	private bool isColliding;

	// Use this for initialization
	void Start () {
		//counter = 0;
	}
	
	// Update is called once per frame
	void Update () {
		isColliding = false;
	}
	
	void OnCollisionEnter(Collision collision) {
		if(isColliding) {
			return;
		}
		isColliding = true;
		
		if(collision.gameObject.tag == "TurretIdling") {
			Destroy(collision.gameObject);
			Destroy(gameObject);
			return;
		}
		
		if(collision.gameObject.tag == "Terrain") {
			Destroy(gameObject);
			GameObject newTurret = (GameObject)Instantiate(turret_prefab, transform.position, Quaternion.identity);
		}
	}
}
