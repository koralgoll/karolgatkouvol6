﻿using UnityEngine;
using System.Collections;

public class TurretIdlingController : MonoBehaviour {

	public GameObject activeTurret_prefab;

	// Use this for initialization
	void Start () {
		StartCoroutine("WaitAndChangeCoroutine");
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	IEnumerator WaitAndChangeCoroutine() {
		yield return new WaitForSeconds(6f);
		
		StopCoroutine("WaitAndChangeCoroutine");
		
		Instantiate(activeTurret_prefab, transform.position, transform.rotation);
		Destroy(gameObject);
		
		
	}
}
