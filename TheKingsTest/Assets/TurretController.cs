﻿using UnityEngine;
using System.Collections;

public class TurretController : MonoBehaviour {

	public GameObject bullet_prefab;
	public int maxFiresNum = 12;
	
	private Transform cannon;
	private int fireCounter = 0;

	// Use this for initialization
	void Start () {
		cannon = gameObject.transform.FindChild("Cannon");
		if(cannon == null) {
			Debug.Log("Could not find Cannon");
		}
	
		StartCoroutine("MyCoroutine");
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	
	IEnumerator MyCoroutine() {
		while(true) {
			Vector3 euler = transform.eulerAngles;
			
			euler.y = Random.Range(15f, 45f);
			transform.eulerAngles = euler;
			
			FireBullet();
			
			if(fireCounter >= maxFiresNum) {
				StopCoroutine("MyCoroutine");
			}
			Debug.Log(fireCounter);
			
			yield return new WaitForSeconds(0.5f);
		}
	}
	
	void FireBullet() {
		float bulletImpulse = Random.Range(4f, 8f);
		Vector3 bulletSpawnPosition = cannon.transform.position;
		bulletSpawnPosition.x += 2f;
		bulletSpawnPosition = Quaternion.Euler(transform.eulerAngles) * bulletSpawnPosition;
		
		GameObject bullet = (GameObject)Instantiate(bullet_prefab, bulletSpawnPosition, transform.rotation);
		bullet.GetComponent<Rigidbody>().AddForce(new Vector3(bulletImpulse, 0f, 0f), ForceMode.Impulse);
		
		++fireCounter;
	}
}
